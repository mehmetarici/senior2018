<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Rbp_list;
use App\Models\Target_gene;

class AnalysisController extends Controller
{
    public function index()
    {
        return view('analysis');
    }

    public function load_example()
    {
        $loaded_rbp = Rbp_list::find(rand(1, 40));
        $target_files = $loaded_rbp->target_genes;
        return view('analysis', compact("loaded_rbp", "target_files"));
    }

    public function post_analyze()
    {
        $this->validate(request(), [
            'rbp_id'            =>  'required|bail|integer|digits_between:1,40',
            'target_file'       =>  'nullable|mimes:txt',
            'background_file'   =>  'nullable|mimes:txt',
            'email'             =>  'nullable|email'
        ]);

        $rbp = Rbp_list::find(request('rbp_id'));
        if (!request('target_file') && !request('background_file'))
        {
            $rbp_name = "\\App\Models\\".ucfirst(strtolower($rbp->rbp_name))."_kmer";
            $rbps = $rbp_name::take(100)->get();
            return view("result", compact("rbps"));
        }
        if (request('target_file'))
        {
            $fileName = $rbp->rbp_name.'_target_genes.txt';
            request('target_file')->storeAs('public/target_background', $fileName);
            Target_gene::create([
                'rbp_id'            =>  request('rbp_id'),
                'isTarget'          =>  1,
                'file_path'         =>  'storage/target_background/'.$fileName,
                'email'             =>  request('email')
            ]);
        }
        if (request('background_file'))
        {
            $fileName = $rbp->rbp_name.'_nontarget_genes.txt';
            request('background_file')->storeAs('public/target_background', $fileName);
            Target_gene::create([
                'rbp_id'            =>  request('rbp_id'),
                'isTarget'          =>  0,
                'file_path'         =>  'storage/target_background/'.$fileName,
                'email'             =>  request('email')
            ]);
        }

         //return redirect()->route('analysis');
    }

}
