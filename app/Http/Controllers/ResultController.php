<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Pum2_kmer;
use App\Models\Table4;

class ResultController extends Controller
{
    public function index(){
        $rbps = Pum2_kmer::take(100)->get();
        return view("result", compact("rbps"));
    }
}
