@extends('app')

@section('css')
    <link rel="stylesheet" href="{{ asset('plugins/datatable/datatables.min.css') }}">
@endsection
@section('js')
    <script type="text/javascript" src="{{ asset('plugins/datatable/datatables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/datatable/ColReorderWithResize.js') }}"></script>
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/data.js"></script>
    <script src="https://code.highcharts.com/modules/drilldown.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script>
        Highcharts.chart('chartDemo', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'ENGS0000874512'
            },
            subtitle: {
                text: 'Click the tissues to view of expressions of the samples.'
            },
            xAxis: {
                type: 'category'
            },
            yAxis: {
                title: {
                    text: 'Y axis'
                }
            },
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0
                }
            },

            tooltip: {
                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <i>{point.y:.2f}</i>'
            },

            "series": [
                {
                    "name": "Browsers",
                    "colorByPoint": true,
                    "data": [
                        {
                            "name": "Chrome",
                            "y": 62.74,
                            "drilldown": "Chrome"
                        },
                        {
                            "name": "Firefox",
                            "y": 10.57,
                            "drilldown": "Firefox"
                        },
                        {
                            "name": "Internet Explorer",
                            "y": 7.23,
                            "drilldown": "Internet Explorer"
                        },
                        {
                            "name": "Safari",
                            "y": 5.58,
                            "drilldown": "Safari"
                        },
                        {
                            "name": "Edge",
                            "y": 4.02,
                            "drilldown": "Edge"
                        },
                        {
                            "name": "Opera",
                            "y": 1.92,
                            "drilldown": "Opera"
                        },
                        {
                            "name": "Other",
                            "y": 7.62,
                            "drilldown": null
                        },
                        {
                            "name": "a",
                            "y": 62.74,
                            "drilldown": "a"
                        },
                        {
                            "name": "b",
                            "y": 10.57,
                            "drilldown": "b"
                        },
                        {
                            "name": "c",
                            "y": 7.23,
                            "drilldown": "c"
                        },
                        {
                            "name": "d",
                            "y": 5.58,
                            "drilldown": "d"
                        },
                        {
                            "name": "e",
                            "y": 4.02,
                            "drilldown": "e"
                        },
                        {
                            "name": "f",
                            "y": 1.92,
                            "drilldown": "f"
                        }
                    ]
                }
            ],
            "drilldown": {
                "series": [
                    {
                        "name": "Chrome",
                        "id": "Chrome",
                        "data": [
                            [
                                "v65.0",
                                0.1
                            ],
                            [
                                "v64.0",
                                1.3
                            ],
                            [
                                "v63.0",
                                53.02
                            ],
                            [
                                "v62.0",
                                1.4
                            ],
                            [
                                "v61.0",
                                0.88
                            ],
                            [
                                "v60.0",
                                0.56
                            ],
                            [
                                "v59.0",
                                0.45
                            ],
                            [
                                "v58.0",
                                0.49
                            ],
                            [
                                "v57.0",
                                0.32
                            ],
                            [
                                "v56.0",
                                0.29
                            ],
                            [
                                "v55.0",
                                0.79
                            ],
                            [
                                "v54.0",
                                0.18
                            ],
                            [
                                "v51.0",
                                0.13
                            ],
                            [
                                "v49.0",
                                2.16
                            ],
                            [
                                "v48.0",
                                0.13
                            ],
                            [
                                "v47.0",
                                0.11
                            ],
                            [
                                "v43.0",
                                0.17
                            ],
                            [
                                "v29.0",
                                0.26
                            ]
                        ]
                    },
                    {
                        "name": "Firefox",
                        "id": "Firefox",
                        "data": [
                            [
                                "v58.0",
                                1.02
                            ],
                            [
                                "v57.0",
                                7.36
                            ],
                            [
                                "v56.0",
                                0.35
                            ],
                            [
                                "v55.0",
                                0.11
                            ],
                            [
                                "v54.0",
                                0.1
                            ],
                            [
                                "v52.0",
                                0.95
                            ],
                            [
                                "v51.0",
                                0.15
                            ],
                            [
                                "v50.0",
                                0.1
                            ],
                            [
                                "v48.0",
                                0.31
                            ],
                            [
                                "v47.0",
                                0.12
                            ]
                        ]
                    },
                    {
                        "name": "Internet Explorer",
                        "id": "Internet Explorer",
                        "data": [
                            [
                                "v11.0",
                                6.2
                            ],
                            [
                                "v10.0",
                                0.29
                            ],
                            [
                                "v9.0",
                                0.27
                            ],
                            [
                                "v8.0",
                                0.47
                            ]
                        ]
                    },
                    {
                        "name": "Safari",
                        "id": "Safari",
                        "data": [
                            [
                                "v11.0",
                                3.39
                            ],
                            [
                                "v10.1",
                                0.96
                            ],
                            [
                                "v10.0",
                                0.36
                            ],
                            [
                                "v9.1",
                                0.54
                            ],
                            [
                                "v9.0",
                                0.13
                            ],
                            [
                                "v5.1",
                                0.2
                            ]
                        ]
                    },
                    {
                        "name": "Edge",
                        "id": "Edge",
                        "data": [
                            [
                                "v16",
                                2.6
                            ],
                            [
                                "v15",
                                0.92
                            ],
                            [
                                "v14",
                                0.4
                            ],
                            [
                                "v13",
                                0.1
                            ]
                        ]
                    },
                    {
                        "name": "Opera",
                        "id": "Opera",
                        "data": [
                            [
                                "v50.0",
                                0.96
                            ],
                            [
                                "v49.0",
                                0.82
                            ],
                            [
                                "v12.1",
                                0.14
                            ]
                        ]
                    },
                    {
                        "name": "a",
                        "id": "a",
                        "data": [
                            [
                                "v50.0",
                                0.96
                            ],
                            [
                                "v49.0",
                                0.82
                            ],
                            [
                                "v12.1",
                                0.14
                            ]
                        ]
                    },
                    {
                        "name": "b",
                        "id": "b",
                        "data": [
                            [
                                "v50.0",
                                0.96
                            ],
                            [
                                "v49.0",
                                0.82
                            ],
                            [
                                "v12.1",
                                0.14
                            ]
                        ]
                    },
                    {
                        "name": "c",
                        "id": "c",
                        "data": [
                            [
                                "v50.0",
                                0.96
                            ],
                            [
                                "v49.0",
                                0.82
                            ],
                            [
                                "v12.1",
                                0.14
                            ]
                        ]
                    },
                    {
                        "name": "d",
                        "id": "d",
                        "data": [
                            [
                                "v50.0",
                                0.96
                            ],
                            [
                                "v49.0",
                                0.82
                            ],
                            [
                                "v12.1",
                                0.14
                            ]
                        ]
                    },
                    {
                        "name": "e",
                        "id": "e",
                        "data": [
                            [
                                "v50.0",
                                0.96
                            ],
                            [
                                "v49.0",
                                0.82
                            ],
                            [
                                "v12.1",
                                0.14
                            ]
                        ]
                    },
                    {
                        "name": "f",
                        "id": "f",
                        "data": [
                            [
                                "v50.0",
                                0.96
                            ],
                            [
                                "v49.0",
                                0.82
                            ],
                            [
                                "v12.1",
                                0.14
                            ]
                        ]
                    }
                ]
            }
        });
        Highcharts.chart('chartDemo2', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'ENGS0000874512'
            },
            subtitle: {
                text: 'Click the tissues to view of expressions of the samples.'
            },
            xAxis: {
                type: 'category'
            },
            yAxis: {
                title: {
                    text: 'Y axis'
                }
            },
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0
                }
            },

            tooltip: {
                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <i>{point.y:.2f}</i>'
            },

            "series": [
                {
                    "name": "Browsers",
                    "colorByPoint": true,
                    "data": [
                        {
                            "name": "Chrome",
                            "y": 62.74,
                            "drilldown": "Chrome"
                        },
                        {
                            "name": "Firefox",
                            "y": 10.57,
                            "drilldown": "Firefox"
                        },
                        {
                            "name": "Internet Explorer",
                            "y": 7.23,
                            "drilldown": "Internet Explorer"
                        },
                        {
                            "name": "Safari",
                            "y": 5.58,
                            "drilldown": "Safari"
                        },
                        {
                            "name": "Edge",
                            "y": 4.02,
                            "drilldown": "Edge"
                        },
                        {
                            "name": "Opera",
                            "y": 1.92,
                            "drilldown": "Opera"
                        },
                        {
                            "name": "Other",
                            "y": 7.62,
                            "drilldown": null
                        },
                        {
                            "name": "a",
                            "y": 62.74,
                            "drilldown": "a"
                        },
                        {
                            "name": "b",
                            "y": 10.57,
                            "drilldown": "b"
                        },
                        {
                            "name": "c",
                            "y": 7.23,
                            "drilldown": "c"
                        },
                        {
                            "name": "d",
                            "y": 5.58,
                            "drilldown": "d"
                        },
                        {
                            "name": "e",
                            "y": 4.02,
                            "drilldown": "e"
                        },
                        {
                            "name": "f",
                            "y": 1.92,
                            "drilldown": "f"
                        }
                    ]
                }
            ],
            "drilldown": {
                "series": [
                    {
                        "name": "Chrome",
                        "id": "Chrome",
                        "data": [
                            [
                                "v65.0",
                                0.1
                            ],
                            [
                                "v64.0",
                                1.3
                            ],
                            [
                                "v63.0",
                                53.02
                            ],
                            [
                                "v62.0",
                                1.4
                            ],
                            [
                                "v61.0",
                                0.88
                            ],
                            [
                                "v60.0",
                                0.56
                            ],
                            [
                                "v59.0",
                                0.45
                            ],
                            [
                                "v58.0",
                                0.49
                            ],
                            [
                                "v57.0",
                                0.32
                            ],
                            [
                                "v56.0",
                                0.29
                            ],
                            [
                                "v55.0",
                                0.79
                            ],
                            [
                                "v54.0",
                                0.18
                            ],
                            [
                                "v51.0",
                                0.13
                            ],
                            [
                                "v49.0",
                                2.16
                            ],
                            [
                                "v48.0",
                                0.13
                            ],
                            [
                                "v47.0",
                                0.11
                            ],
                            [
                                "v43.0",
                                0.17
                            ],
                            [
                                "v29.0",
                                0.26
                            ]
                        ]
                    },
                    {
                        "name": "Firefox",
                        "id": "Firefox",
                        "data": [
                            [
                                "v58.0",
                                1.02
                            ],
                            [
                                "v57.0",
                                7.36
                            ],
                            [
                                "v56.0",
                                0.35
                            ],
                            [
                                "v55.0",
                                0.11
                            ],
                            [
                                "v54.0",
                                0.1
                            ],
                            [
                                "v52.0",
                                0.95
                            ],
                            [
                                "v51.0",
                                0.15
                            ],
                            [
                                "v50.0",
                                0.1
                            ],
                            [
                                "v48.0",
                                0.31
                            ],
                            [
                                "v47.0",
                                0.12
                            ]
                        ]
                    },
                    {
                        "name": "Internet Explorer",
                        "id": "Internet Explorer",
                        "data": [
                            [
                                "v11.0",
                                6.2
                            ],
                            [
                                "v10.0",
                                0.29
                            ],
                            [
                                "v9.0",
                                0.27
                            ],
                            [
                                "v8.0",
                                0.47
                            ]
                        ]
                    },
                    {
                        "name": "Safari",
                        "id": "Safari",
                        "data": [
                            [
                                "v11.0",
                                3.39
                            ],
                            [
                                "v10.1",
                                0.96
                            ],
                            [
                                "v10.0",
                                0.36
                            ],
                            [
                                "v9.1",
                                0.54
                            ],
                            [
                                "v9.0",
                                0.13
                            ],
                            [
                                "v5.1",
                                0.2
                            ]
                        ]
                    },
                    {
                        "name": "Edge",
                        "id": "Edge",
                        "data": [
                            [
                                "v16",
                                2.6
                            ],
                            [
                                "v15",
                                0.92
                            ],
                            [
                                "v14",
                                0.4
                            ],
                            [
                                "v13",
                                0.1
                            ]
                        ]
                    },
                    {
                        "name": "Opera",
                        "id": "Opera",
                        "data": [
                            [
                                "v50.0",
                                0.96
                            ],
                            [
                                "v49.0",
                                0.82
                            ],
                            [
                                "v12.1",
                                0.14
                            ]
                        ]
                    },
                    {
                        "name": "a",
                        "id": "a",
                        "data": [
                            [
                                "v50.0",
                                0.96
                            ],
                            [
                                "v49.0",
                                0.82
                            ],
                            [
                                "v12.1",
                                0.14
                            ]
                        ]
                    },
                    {
                        "name": "b",
                        "id": "b",
                        "data": [
                            [
                                "v50.0",
                                0.96
                            ],
                            [
                                "v49.0",
                                0.82
                            ],
                            [
                                "v12.1",
                                0.14
                            ]
                        ]
                    },
                    {
                        "name": "c",
                        "id": "c",
                        "data": [
                            [
                                "v50.0",
                                0.96
                            ],
                            [
                                "v49.0",
                                0.82
                            ],
                            [
                                "v12.1",
                                0.14
                            ]
                        ]
                    },
                    {
                        "name": "d",
                        "id": "d",
                        "data": [
                            [
                                "v50.0",
                                0.96
                            ],
                            [
                                "v49.0",
                                0.82
                            ],
                            [
                                "v12.1",
                                0.14
                            ]
                        ]
                    },
                    {
                        "name": "e",
                        "id": "e",
                        "data": [
                            [
                                "v50.0",
                                0.96
                            ],
                            [
                                "v49.0",
                                0.82
                            ],
                            [
                                "v12.1",
                                0.14
                            ]
                        ]
                    },
                    {
                        "name": "f",
                        "id": "f",
                        "data": [
                            [
                                "v50.0",
                                0.96
                            ],
                            [
                                "v49.0",
                                0.82
                            ],
                            [
                                "v12.1",
                                0.14
                            ]
                        ]
                    }
                ]
            }
        });
        Highcharts.chart('chartDemo3', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'hsa-miR-654-5p'
            },
            subtitle: {
                text: 'Click the tissues to view of expressions of the samples.'
            },
            xAxis: {
                type: 'category'
            },
            yAxis: {
                title: {
                    text: 'Y axis'
                }
            },
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0
                }
            },

            tooltip: {
                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <i>{point.y:.2f}</i>'
            },

            "series": [
                {
                    "name": "Browsers",
                    "colorByPoint": true,
                    "data": [
                        {
                            "name": "Chrome",
                            "y": 62.74,
                            "drilldown": "Chrome"
                        },
                        {
                            "name": "Firefox",
                            "y": 10.57,
                            "drilldown": "Firefox"
                        },
                        {
                            "name": "Internet Explorer",
                            "y": 7.23,
                            "drilldown": "Internet Explorer"
                        },
                        {
                            "name": "Safari",
                            "y": 5.58,
                            "drilldown": "Safari"
                        },
                        {
                            "name": "Edge",
                            "y": 4.02,
                            "drilldown": "Edge"
                        },
                        {
                            "name": "Opera",
                            "y": 1.92,
                            "drilldown": "Opera"
                        },
                        {
                            "name": "Other",
                            "y": 7.62,
                            "drilldown": null
                        },
                        {
                            "name": "a",
                            "y": 62.74,
                            "drilldown": "a"
                        },
                        {
                            "name": "b",
                            "y": 10.57,
                            "drilldown": "b"
                        },
                        {
                            "name": "c",
                            "y": 7.23,
                            "drilldown": "c"
                        },
                        {
                            "name": "d",
                            "y": 5.58,
                            "drilldown": "d"
                        },
                        {
                            "name": "e",
                            "y": 4.02,
                            "drilldown": "e"
                        },
                        {
                            "name": "f",
                            "y": 1.92,
                            "drilldown": "f"
                        }
                    ]
                }
            ],
            "drilldown": {
                "series": [
                    {
                        "name": "Chrome",
                        "id": "Chrome",
                        "data": [
                            [
                                "v65.0",
                                0.1
                            ],
                            [
                                "v64.0",
                                1.3
                            ],
                            [
                                "v63.0",
                                53.02
                            ],
                            [
                                "v62.0",
                                1.4
                            ],
                            [
                                "v61.0",
                                0.88
                            ],
                            [
                                "v60.0",
                                0.56
                            ],
                            [
                                "v59.0",
                                0.45
                            ],
                            [
                                "v58.0",
                                0.49
                            ],
                            [
                                "v57.0",
                                0.32
                            ],
                            [
                                "v56.0",
                                0.29
                            ],
                            [
                                "v55.0",
                                0.79
                            ],
                            [
                                "v54.0",
                                0.18
                            ],
                            [
                                "v51.0",
                                0.13
                            ],
                            [
                                "v49.0",
                                2.16
                            ],
                            [
                                "v48.0",
                                0.13
                            ],
                            [
                                "v47.0",
                                0.11
                            ],
                            [
                                "v43.0",
                                0.17
                            ],
                            [
                                "v29.0",
                                0.26
                            ]
                        ]
                    },
                    {
                        "name": "Firefox",
                        "id": "Firefox",
                        "data": [
                            [
                                "v58.0",
                                1.02
                            ],
                            [
                                "v57.0",
                                7.36
                            ],
                            [
                                "v56.0",
                                0.35
                            ],
                            [
                                "v55.0",
                                0.11
                            ],
                            [
                                "v54.0",
                                0.1
                            ],
                            [
                                "v52.0",
                                0.95
                            ],
                            [
                                "v51.0",
                                0.15
                            ],
                            [
                                "v50.0",
                                0.1
                            ],
                            [
                                "v48.0",
                                0.31
                            ],
                            [
                                "v47.0",
                                0.12
                            ]
                        ]
                    },
                    {
                        "name": "Internet Explorer",
                        "id": "Internet Explorer",
                        "data": [
                            [
                                "v11.0",
                                6.2
                            ],
                            [
                                "v10.0",
                                0.29
                            ],
                            [
                                "v9.0",
                                0.27
                            ],
                            [
                                "v8.0",
                                0.47
                            ]
                        ]
                    },
                    {
                        "name": "Safari",
                        "id": "Safari",
                        "data": [
                            [
                                "v11.0",
                                3.39
                            ],
                            [
                                "v10.1",
                                0.96
                            ],
                            [
                                "v10.0",
                                0.36
                            ],
                            [
                                "v9.1",
                                0.54
                            ],
                            [
                                "v9.0",
                                0.13
                            ],
                            [
                                "v5.1",
                                0.2
                            ]
                        ]
                    },
                    {
                        "name": "Edge",
                        "id": "Edge",
                        "data": [
                            [
                                "v16",
                                2.6
                            ],
                            [
                                "v15",
                                0.92
                            ],
                            [
                                "v14",
                                0.4
                            ],
                            [
                                "v13",
                                0.1
                            ]
                        ]
                    },
                    {
                        "name": "Opera",
                        "id": "Opera",
                        "data": [
                            [
                                "v50.0",
                                0.96
                            ],
                            [
                                "v49.0",
                                0.82
                            ],
                            [
                                "v12.1",
                                0.14
                            ]
                        ]
                    },
                    {
                        "name": "a",
                        "id": "a",
                        "data": [
                            [
                                "v50.0",
                                0.96
                            ],
                            [
                                "v49.0",
                                0.82
                            ],
                            [
                                "v12.1",
                                0.14
                            ]
                        ]
                    },
                    {
                        "name": "b",
                        "id": "b",
                        "data": [
                            [
                                "v50.0",
                                0.96
                            ],
                            [
                                "v49.0",
                                0.82
                            ],
                            [
                                "v12.1",
                                0.14
                            ]
                        ]
                    },
                    {
                        "name": "c",
                        "id": "c",
                        "data": [
                            [
                                "v50.0",
                                0.96
                            ],
                            [
                                "v49.0",
                                0.82
                            ],
                            [
                                "v12.1",
                                0.14
                            ]
                        ]
                    },
                    {
                        "name": "d",
                        "id": "d",
                        "data": [
                            [
                                "v50.0",
                                0.96
                            ],
                            [
                                "v49.0",
                                0.82
                            ],
                            [
                                "v12.1",
                                0.14
                            ]
                        ]
                    },
                    {
                        "name": "e",
                        "id": "e",
                        "data": [
                            [
                                "v50.0",
                                0.96
                            ],
                            [
                                "v49.0",
                                0.82
                            ],
                            [
                                "v12.1",
                                0.14
                            ]
                        ]
                    },
                    {
                        "name": "f",
                        "id": "f",
                        "data": [
                            [
                                "v50.0",
                                0.96
                            ],
                            [
                                "v49.0",
                                0.82
                            ],
                            [
                                "v12.1",
                                0.14
                            ]
                        ]
                    }
                ]
            }
        });
    </script>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <table id="example" class="display table table-bordered table-hover" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th style="width: 180px">LncRNA</th>
                    <th>Gene Name</th>
                    <th>Motifs Num.</th>
                    <th>Motifs Num. in CLIP</th>
                    <th>LOD Score</th>
                    <th>Dispersity</th>
                    <th>CLIPdb CLIP</th>
                    <th>EMTAB2706 Median</th>
                    <th>EMTAB2706 Max</th>
                    <th>EMTAB2770 Median</th>
                    <th>EMTAB2770 Max</th>
                    <th>GTEX Median</th>
                    <th>GTEX Max</th>
                    <th class="graph-th">Graph</th>
                    <th class="button-in-table"></th>
                    <th class="none">Binding Sites: </th>
                    <th class="none">Exons (-): </th>
                </tr>
                </thead>
                <tbody>
                @php $i = 0 @endphp
                @foreach ($rbps as $rbp)
                    @php
                        $result = App\Models\Table4::where("transcript_id", $rbp->lncRNA_id)->first();
                        $i++;
                    @endphp
                    <tr>
                        <td class="transcript_id">{{ $rbp->lncRNA_id }} </td>
                        <td class="kmers">{{ $result->gene_name }}</td>
                        <td>{{ $rbp->num_motifs }}</td>
                        <td>{{ $rbp->num_motifs_in_CLIP_peaks }}</td>
                        <td>{{ $rbp->LOD_score }}</td>
                        <td>{{ $rbp->disp_score }}</td>
                        <td>{{ $rbp->num_eCLIP_peaks }}</td>
                        <td>{{ $rbp->EMTAB2706_med_exp }}</td>
                        <td>{{ $rbp->EMTAB2706_max_exp }}</td>
                        <td>{{ $rbp->EMTAB2770_med_exp }}</td>
                        <td>{{ $rbp->EMTAB2770_max_exp }}</td>
                        <td>{{ $rbp->GTEX_max_exp }}</td>
                        <td>{{ $rbp->GTEX_max_exp }}</td>
                        <td class="genome-cell">
                            <svg id="genome{{ $rbp->lncRNA_id }}" class="genome-in-table" width="100%" height="20"></svg>
                            <span class="hidden rangeLncRNA">{{ $result->transcript_epos - $result->transcript_spos }}</span>
                            <span class="hidden motifList">{{ $rbp->motif_list }}</span>
                        </td>
                        <td class="iconned-td">
                            <a data-toggle="modal" style="margin-right: 7px;" data-target="#genomeviewer"><i class="fa fa-eye"></i></a>
                            <a data-toggle="modal" style="color: #880E4F;" data-target="#chart"><i class="far fa-chart-bar"></i></a>
                        </td>
                        <td class="genome-cell">
                            <svg id="bigGenome{{ $rbp->lncRNA_id }}" class="genome-in-table" width="100%" height="20"></svg>
                        </td>
                        <td>$320,800</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="genomeviewer" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="exampleModalLongTitle">Genome Viewer</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    ...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="chart" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3>Expression Table</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"><i class="fa fa-times-circle"></i></span>
                    </button>
                </div>
                <div class="modal-body" style="height: 450px;">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="tabbable">
                                    <ul class="nav tab-group"><!--nav-tabs nav-justified-->
                                        <li class="active"><a href="#about" data-toggle="tab">ENGS0000874512</a></li>
                                        <li><a href="#social" data-toggle="tab">hsa-miR-541-3p</a></li>
                                        <li><a href="#location" data-toggle="tab">hsa-miR-654-5p</a></li>
                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="about">
                                            <div id="chartDemo" style="width:100%; height:350px;"></div>
                                        </div>
                                        <div class="tab-pane" id="social">
                                            <div id="chartDemo2" style="width:100%; height:350px;"></div>
                                        </div>
                                        <div class="tab-pane" id="location">
                                            <div id="chartDemo3" style="width:100%; height:350px;"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
