@extends('app')

@section('section')

@endsection
@section('js')

@endsection

@section('content')
    <div class="row">
        <h1 class="text-center">Welcome to BindScan!</h1>
        <p class="text-center">spongeScan is a web tool designed to identify and visualize lncRNAs acting as putative miRNA sponges, by searching for multiple miRNA binding sites in lncRNAs.</p>
    </div>
    <div class="row">
        <div class="col-md-12">
            <img src="{{ asset('img/contents/home1.png') }}" alt="">
        </div>
    </div>
@endsection